const path = require('path')

const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const helmet = require('helmet')

const postRoutes = require('./lib/routes/post')
const categoryRoutes = require('./lib/routes/category')

const app = express()

const MONGO_URI = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@gogogood-asc0n.mongodb.net/${process.env.MONGO_DB}?retryWrites=true`

// const privateKey = fs.readFileSync('server.key')
// const certificate = fs.readFileSync('server.cert')

app.use(bodyParser.json()) // application/json

app.use('/images', express.static(path.join(__dirname, 'images')))

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PATCH, DELETE'
  )
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  next()
})

// ROUTES

app.use('/posts', postRoutes)
app.use('/categories', categoryRoutes)

app.use((error, req, res, next) => {
  console.log(error)
  const status = error.statusCode || 500
  const message = error.message
  const data = error.data
  res.status(status).json({ message: message, data: data })
})

app.use(helmet())

mongoose.connect(MONGO_URI, { useNewUrlParser: true })
  .then(result => {
    // https
    //   .createServer({
    //     key: privateKey,
    //     cert: certificate
    //   } ,app)
    app.listen(process.env.PORT || 8080, '0.0.0.0')
  })
  .catch(err => console.log(err))
