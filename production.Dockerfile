FROM node:8.16-alpine

RUN mkdir /app

WORKDIR /app

COPY /app.js ./
COPY /lib ./lib
COPY ./package.json ./

RUN npm install

EXPOSE 8080

CMD ["npm", "run", "start"]