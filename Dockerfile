FROM node:8.16-alpine

RUN mkdir /app

RUN npm i -g nodemon

WORKDIR /app

CMD ["npm", "run", "start:dev"]
