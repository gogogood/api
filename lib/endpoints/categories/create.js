const { validationResult } = require('express-validator/check')

const Category = require('../../models/category')

exports.create = async (req, res, next) => {
  try {
    const displayValue = req.body.displayValue
    const slug = req.body.slug
    const extension = req.body.extension

    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const error = new Error('Validation failed, entered data is incorrect.')
      error.statusCode = 422
      error.data = errors.array()
      throw error
    }

    const category = new Category({
      displayValue: displayValue,
      slug: slug,
      extension: extension
    })

    const newRecord = await category.save()

    if (!newRecord) {
      const err = new Error('Could not find post')
      err.statusCode = 404
      throw err
    }

    res.status(201).json({
      message: 'Yes ! It works',
      category: newRecord
    })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}
