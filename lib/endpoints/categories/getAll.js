const Category = require('../../models/category')

exports.getAll = async (req, res, next) => {
  try {
    const resultFind = await Category.find()

    if (!resultFind) {
      const err = new Error('Could not find post')
      err.statusCode = 404
      throw err
    }

    res.status(200).json({
      message: 'Categories fetched successfully !',
      categories: resultFind
    })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}
