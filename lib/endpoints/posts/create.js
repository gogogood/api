const { validationResult } = require('express-validator/check')

const Category = require('../../models/category')
const Post = require('../../models/post')

exports.create = async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const error = new Error('Validation failed, entered data is incorrect.')
      error.statusCode = 422
      error.data = errors.array()
      throw error
    }

    const name = req.body.name
    const street = req.body.street
    const district = req.body.district
    const city = req.body.city
    const country = req.body.country
    const googleMapUrl = req.body.googleMapUrl
    const phone = req.body.phone
    const openningHours = req.body.openningHours
    const category = req.body.category
    const catchPhrase = req.body.catchPhrase
    const description = req.body.description
    const plateName1 = req.body.plateName1
    const platePrice1 = req.body.platePrice1
    const plateDescription1 = req.body.plateDescription1
    const plateName2 = req.body.plateName2
    const platePrice2 = req.body.platePrice2
    const plateDescription2 = req.body.plateDescription2
    const plateName3 = req.body.plateName3
    const platePrice3 = req.body.platePrice3
    const plateDescription3 = req.body.plateDescription3
    const rate = req.body.rate
    const photoPola = req.body.photoPola
    const Photo1 = req.body.Photo1
    const Photo2 = req.body.Photo2
    const Photo3 = req.body.Photo3

    const categoriesFetched = await Category.find()

    let categoryId

    categoriesFetched.map(ctgy => {
      if (ctgy.slug === category) {
        categoryId = ctgy._id
      }
    })

    const post = new Post({
      name: name,
      street: street,
      district: district,
      city: city,
      country: country,
      googleMapUrl: googleMapUrl,
      phone: phone,
      openningHours: openningHours,
      category: category,
      categoryId: categoryId,
      catchPhrase: catchPhrase,
      description: description,
      plateName1: plateName1,
      platePrice1: platePrice1,
      plateDescription1: plateDescription1,
      plateName2: plateName2,
      platePrice2: platePrice2,
      plateDescription2: plateDescription2,
      plateName3: plateName3,
      platePrice3: platePrice3,
      plateDescription3: plateDescription3,
      rate: rate,
      photoPola: photoPola,
      Photo1: Photo1,
      Photo2: Photo2,
      Photo3: Photo3
    })
    const recordCreated = await post.save()

    if (!recordCreated) {
      const err = new Error('Could create post')
      err.statusCode = 404
      throw err
    }
    res.status(201).json({
      message: 'Yes ! It works',
      restaurant: recordCreated
    })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}

// exports.createPost = (req, res, next) => {
//   const errors = validationResult(req);
//   if (!errors.isEmpty()) {
//     const error = new Error('Validation failed, entered data is incorrect.');
//     error.statusCode = 422;
//     throw error;
//   }
//   if (!req.file) {
//     const error = new Error('No image provided.');
//     error.statusCode = 422;
//     throw error;
//   }
//   const imageUrl = req.file.path;
//   const title = req.body.title;
//   const content = req.body.content;
//   let creator;
//   const post = new Post({
//     title: title,
//     content: content,
//     imageUrl: imageUrl,
//     creator: req.userId
//   });
//   post
//     .save()
//     .then(result => {
//       return User.findById(req.userId);
//     })
//     .then(user => {
//       creator = user;
//       user.posts.push(post);
//       return user.save();
//     })
//     .then(result => {
//       res.status(201).json({
//         message: 'Post created successfully!',
//         post: post,
//         creator: { _id: creator._id, name: creator.name }
//       });
//     })
//     .catch(err => {
//       if (!err.statusCode) {
//         err.statusCode = 500;
//       }
//       next(err);
//     });
// };
