const Post = require('../../models/post')

exports.getAllByCategory = async (req, res, next) => {
  try {
    const postCategory = req.params.category
    console.log('postCategory =>', postCategory)
    const postsFetched = await Post.find()
    console.log('postsFetched =>', postsFetched)
    const postsNeeded = postsFetched.filter(post => post.category === postCategory)

    console.log('postsNeeded =>', postsNeeded)
    if (!postsNeeded || postsNeeded.length === 0) {
      const error = new Error(`Could not find posts in ${postCategory} .`)
      error.statusCode = 404
      throw error
    }
    res.status(200).json({
      message: `Posts from ${postCategory} fetched.`,
      posts: postsNeeded
    })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}
