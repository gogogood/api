const Post = require('../../models/post')

exports.delete = async (req, res, next) => {
  const postId = req.params.postId

  const post = await Post.findById(postId)

  if (!post) {
    const error = new Error('Could not find restaurant.')
    error.statusCode = 404
    throw error
  }

  const postDeleted = await Post.findByIdAndRemove(postId)

  console.log(postDeleted)
  res.status(200).json({ message: 'Restaurant deleted.' })
}
