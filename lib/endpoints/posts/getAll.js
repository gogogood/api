const Post = require('../../models/post')

exports.getAll = async (req, res, next) => {
  try {
    const resultFind = await Post.find()

    if (!resultFind) {
      const err = new Error('Could not find post')
      err.statusCode = 404
      throw err
    }
    console.log(resultFind)
    res.status(200).json({
      message: 'Restaurants fetched successfully !',
      restaurants: resultFind
    })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}
