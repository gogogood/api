const { validationResult } = require('express-validator/check')

const Category = require('../../models/category')
const Post = require('../../models/post')

exports.update = async (req, res, next) => {
  try {
    const postId = req.params.postId
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      const error = new Error('Validation failed, entered data is incorrect.')
      error.statusCode = 422
      error.data = errors.array()
      throw error
    }

    const name = req.body.name
    const street = req.body.street
    const district = req.body.district
    const city = req.body.city
    const country = req.body.country
    const googleMapUrl = req.body.googleMapUrl
    const phone = req.body.phone
    const openningHours = req.body.openningHours
    const category = req.body.category
    const catchPhrase = req.body.catchPhrase
    const description = req.body.description
    const plateName1 = req.body.plateName1
    const platePrice1 = req.body.platePrice1
    const plateDescription1 = req.body.plateDescription1
    const plateName2 = req.body.plateName2
    const platePrice2 = req.body.platePrice2
    const plateDescription2 = req.body.plateDescription2
    const plateName3 = req.body.plateName3
    const platePrice3 = req.body.platePrice3
    const plateDescription3 = req.body.plateDescription3
    const rate = req.body.rate
    const photoPola = req.body.photoPola
    const Photo1 = req.body.Photo1
    const Photo2 = req.body.Photo2
    const Photo3 = req.body.Photo3

    const postToUpdate = await Post.findById(postId)
    const categoriesFetched = await Category.find()

    if (!postToUpdate) {
      const error = new Error('Could not find the post you looked for')
      error.statusCode = 404
      throw error
    }

    let categoryId

    if (postToUpdate.category !== category) {
      categoriesFetched.map(ctgy => {
        if (ctgy.slug === category) {
          categoryId = ctgy._id
        }
      })
    }

    postToUpdate.name = name
    postToUpdate.street = street
    postToUpdate.district = district
    postToUpdate.city = city
    postToUpdate.country = country
    postToUpdate.googleMapUrl = googleMapUrl
    postToUpdate.phone = phone
    postToUpdate.openningHours = openningHours
    postToUpdate.category = category
    postToUpdate.categoryId = categoryId
    postToUpdate.catchPhrase = catchPhrase
    postToUpdate.description = description
    postToUpdate.plateName1 = plateName1
    postToUpdate.platePrice1 = platePrice1
    postToUpdate.plateDescription1 = plateDescription1
    postToUpdate.plateName2 = plateName2
    postToUpdate.platePrice2 = platePrice2
    postToUpdate.plateDescription2 = plateDescription2
    postToUpdate.plateName3 = plateName3
    postToUpdate.platePrice3 = platePrice3
    postToUpdate.plateDescription3 = plateDescription3
    postToUpdate.rate = rate
    postToUpdate.photoPola = photoPola
    postToUpdate.Photo1 = Photo1
    postToUpdate.Photo2 = Photo2
    postToUpdate.Photo3 = Photo3

    const postUpdated = await postToUpdate.save()

    if (!postUpdated) {
      const error = new Error('Could not update the post')
      error.statusCode = 404
      throw error
    }

    res.status(200).json({
      message: 'Post updated!',
      post: postUpdated
    })
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500
    }
    next(err)
  }
}
