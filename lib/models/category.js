const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categorySchema = new Schema(
  {
    displayValue: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      required: true
    },
    extension: {
      type: String,
      required: true
    }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Category', categorySchema)
