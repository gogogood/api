const mongoose = require('mongoose')
const Schema = mongoose.Schema

const restaurantSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    street: {
      type: String,
      required: true
    },
    district: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    country: {
      type: String,
      required: true
    },
    googleMapUrl: {
      type: String,
      required: true
    },
    phone: {
      type: String,
      required: true
    },
    openningHours: {
      type: String,
      required: true
    },
    category: {
      type: String,
      required: true
    },
    categoryId: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
      required: false
    },
    catchPhrase: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    plateName1: {
      type: String,
      required: true
    },
    platePrice1: {
      type: String,
      required: true
    },
    plateDescription1: {
      type: String,
      required: true
    },
    plateName2: {
      type: String,
      required: false
    },
    platePrice2: {
      type: String,
      required: false
    },
    plateDescription2: {
      type: String,
      required: false
    },
    plateName3: {
      type: String,
      required: false
    },
    platePrice3: {
      type: String,
      required: false
    },
    plateDescription3: {
      type: String,
      required: false
    },
    rate: {
      type: String,
      required: true
    },
    photoPola: {
      type: String,
      required: true
    },
    Photo1: {
      type: String,
      required: true
    },
    Photo2: {
      type: String,
      required: true
    },
    Photo3: {
      type: String,
      required: true
    }
    // creator: {
    //   type: Schema.Types.ObjectId,
    //   ref: 'User',
    //   required: true
    // }
  },
  { timestamps: true }
)

module.exports = mongoose.model('Restaurant', restaurantSchema)
