const express = require('express')
const { body } = require('express-validator/check')

const categoryCreate = require('../endpoints/categories/create')
const categoryGetAll = require('../endpoints/categories/getAll')

const router = express.Router()

router.post(
  '/create',
  [
    body('displayValue').not().isEmpty(),
    body('slug').not().isEmpty()
  ],
  categoryCreate.create
)

router.get('/all', categoryGetAll.getAll)

module.exports = router
