const express = require('express')
const { body } = require('express-validator/check')

const postsCreate = require('../endpoints/posts/create')
const postsGetAll = require('../endpoints/posts/getAll')
const postsGetOne = require('../endpoints/posts/getOne')
const postsUpdate = require('../endpoints/posts/update')
const postsDelete = require('../endpoints/posts/delete')
const postsGetAllByCategory = require('../endpoints/posts/getPostsByCategory')
// const isAuth = require('../middleware/is-auth')

const router = express.Router()

// GET /feed/posts
// router.get('/posts', isAuth, feedController.getPosts)
router.post(
  '/create',
  [
    body('name').not().isEmpty(),
    body('street').not().isEmpty(),
    body('district').not().isEmpty(),
    body('city').not().isEmpty(),
    body('country').not().isEmpty(),
    body('googleMapUrl').not().isEmpty(),
    body('phone').not().isEmpty(),
    body('openningHours').not().isEmpty(),
    body('category').not().isEmpty(),
    body('catchPhrase').isLength({ min: 35, max: 63 }).not().isEmpty(),
    body('description').isLength({ max: 791 }).not().isEmpty(),
    body('plateName1').isLength({ max: 23 }).not().isEmpty(),
    body('platePrice1').not().isEmpty(),
    body('plateDescription1').isLength({ max: 79 }).not().isEmpty(),
    body('plateName2').isLength({ max: 23 }).not().isEmpty(),
    body('platePrice2').not().isEmpty(),
    body('plateDescription2').isLength({ max: 79 }).not().isEmpty(),
    body('plateName3').isLength({ max: 23 }).not().isEmpty(),
    body('platePrice3').not().isEmpty(),
    body('plateDescription3').isLength({ max: 79 }).not().isEmpty(),
    body('rate').not().isEmpty(),
    body('photoPola').not().isEmpty(),
    body('Photo1').not().isEmpty(),
    body('Photo2').not().isEmpty(),
    body('Photo3').not().isEmpty()
  ],
  postsCreate.create)

router.get('/all', postsGetAll.getAll)

router.get('/district/:category', postsGetAllByCategory.getAllByCategory)
// // POST /feed/post
// router.post(
//   '/post',
//   isAuth,
//   [
//     body('title')
//       .trim()
//       .isLength({ min: 5 }),
//     body('content')
//       .trim()
//       .isLength({ min: 5 })
//   ],
//   feedController.createPost
// )

router.put(
  '/:postId',
  [
    body('name').not().isEmpty(),
    body('street').not().isEmpty(),
    body('district').not().isEmpty(),
    body('city').not().isEmpty(),
    body('country').not().isEmpty(),
    body('googleMapUrl').not().isEmpty(),
    body('phone').not().isEmpty(),
    body('openningHours').not().isEmpty(),
    body('category').not().isEmpty(),
    body('catchPhrase').isLength({ min: 35, max: 63 }).not().isEmpty(),
    body('description').isLength({ max: 791 }).not().isEmpty(),
    body('plateName1').isLength({ max: 23 }).not().isEmpty(),
    body('platePrice1').not().isEmpty(),
    body('plateDescription1').isLength({ max: 79 }).not().isEmpty(),
    body('plateName2').isLength({ max: 23 }).not().isEmpty(),
    body('platePrice2').not().isEmpty(),
    body('plateDescription2').isLength({ max: 79 }).not().isEmpty(),
    body('plateName3').isLength({ max: 23 }).not().isEmpty(),
    body('platePrice3').not().isEmpty(),
    body('plateDescription3').isLength({ max: 79 }).not().isEmpty(),
    body('rate').not().isEmpty(),
    body('photoPola').not().isEmpty(),
    body('Photo1').not().isEmpty(),
    body('Photo2').not().isEmpty(),
    body('Photo3').not().isEmpty()
  ],
  postsUpdate.update)

router.get('/:postId', postsGetOne.getOne)

router.delete('/:postId', postsDelete.delete)

module.exports = router
